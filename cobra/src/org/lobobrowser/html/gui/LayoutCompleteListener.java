package org.lobobrowser.html.gui;

import java.util.EventListener;
import java.util.EventObject;

/**
 * Receives selection change events.
 * @author upasaka Khantibalo
 */
public interface LayoutCompleteListener extends EventListener {
	
	public void LayoutComplete(EventObject event);

}
