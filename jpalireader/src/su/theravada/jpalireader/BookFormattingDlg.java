package su.theravada.jpalireader;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JCheckBox;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import su.theravada.jpalireader.util.SqlUtil;
import su.theravada.jpalireader.util.UTF8Control;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class BookFormattingDlg extends javax.swing.JDialog {
	public JCheckBox ctlShowBold;
	private JButton ctlOK;
	private JButton ctlCancel;
	public JCheckBox ctlShowFont;
	private int _nodeID;
	private int _fileID;
	public boolean DialogSuccess;
	
	public BookFormattingDlg(JFrame frame,int NodeID) {
		super(frame);
		_nodeID=NodeID;
		initGUI();
		FillData();
	}
	
	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				this.setPreferredSize(new java.awt.Dimension(332, 129));
				this.setResizable(false);
				this.setModal(true);
			}
			ResourceBundle res_strings = ResourceBundle.getBundle("strings", Locale.getDefault(),new UTF8Control());
			{
				ctlShowBold = new JCheckBox();
				getContentPane().add(ctlShowBold);
				ctlShowBold.setText(res_strings.getString("EnableBoldText"));
				ctlShowBold.setBounds(7, 0, 305, 23);
			}
			{
				ctlShowFont = new JCheckBox();
				getContentPane().add(ctlShowFont);
				ctlShowFont.setText(res_strings.getString("EnableFontFormatting"));
				ctlShowFont.setBounds(7, 24, 305, 19);
			}
			{
				ctlOK = new JButton();
				getContentPane().add(ctlOK);
				ctlOK.setText(res_strings.getString("OK"));
				ctlOK.setBounds(58, 59, 104, 22);
				ctlOK.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						ctlOKActionPerformed(evt);
					}
				});
			}
			{
				ctlCancel = new JButton();
				getContentPane().add(ctlCancel);
				ctlCancel.setText(res_strings.getString("Cancel"));
				ctlCancel.setBounds(191, 59, 121, 22);
				ctlCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						DialogSuccess=false;
						setVisible(false);
					}
				});
			}
			this.setSize(332, 129);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void FillData()
	{
        ResultSet rs = null;
        PreparedStatement psSelect = null;
        
		try
		{			
            psSelect=MainWindow.getDBConnection().prepareStatement("SELECT files.showbold,files.showfont,files.FileID FROM files INNER JOIN nodes ON files.FileID=nodes.FileID WHERE NodeID=?");
            psSelect.setInt(1, _nodeID);
            
            rs = psSelect.executeQuery();
            if(rs.next())
            {
            	ctlShowBold.setSelected(rs.getBoolean("showbold"));
            	ctlShowFont.setSelected(rs.getBoolean("showfont"));
            	_fileID=rs.getInt("FileID");
            }                         
		}
		catch(SQLException sqle)
		{		
			SqlUtil.ReportSQLException(sqle);
		}
		catch(Exception ex)
		{		
			ex.printStackTrace();					
		}
		finally
		{
			SqlUtil.ClosePreparedStatement(psSelect);
			SqlUtil.CloseResultSet(rs);		
		}
	}
	
	private void ctlOKActionPerformed(ActionEvent evt) {

        PreparedStatement psUpdate = null;
        
		try
		{			
            psUpdate=MainWindow.getDBConnection().prepareStatement("UPDATE files SET showbold=?,showfont=? WHERE FileID=?");
            psUpdate.setBoolean(1, ctlShowBold.isSelected());
            psUpdate.setBoolean(2, ctlShowFont.isSelected());
            psUpdate.setInt(3, _fileID);
            
            psUpdate.execute();                    
		}
		catch(SQLException sqle)
		{		
			SqlUtil.ReportSQLException(sqle);
		}
		catch(Exception ex)
		{		
			ex.printStackTrace();					
		}
		finally
		{
			SqlUtil.ClosePreparedStatement(psUpdate);	
		}
				
		DialogSuccess=true;
		setVisible(false);
	}

}
