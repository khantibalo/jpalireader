package su.theravada.jpalireader;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ListDataEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

public class PaliComboBox extends JComboBox implements PopupMenuListener {
	private JTextField _textField;
	private JTextArea _textArea;
	
	public PaliComboBox()
	{
		try
		{
			PaliKey[] arrPaliKeys=new PaliKey[] {
				new PaliKey("ā","ā - Alt+a"),
				new PaliKey("ī","ī - Alt+i"),
				new PaliKey("ū","ū - Alt+u"),
				new PaliKey("ṭ","ṭ - Alt+t"),
				new PaliKey("ñ","ñ - Alt+n"),
				new PaliKey("ṃ","ṃ - Alt+m"),
				new PaliKey("ṇ","ṇ - Alt+g"),
				new PaliKey("ṅ","ṅ - Alt+j"),
				new PaliKey("ḷ","ḷ - Alt+l"),
				new PaliKey("ḍ","ḍ - Alt+d")			
			};
			
			ComboBoxModel ctlSearchModeModel = new DefaultComboBoxModel(arrPaliKeys);
			setModel(ctlSearchModeModel);			
			
			addPopupMenuListener(this);			
			setSelectedItem(null);
			setMaximumRowCount(10);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	//this is for items list to be wider
    private boolean layingOut = false; 

    public void doLayout(){ 
        try{ 
            layingOut = true; 
                super.doLayout(); 
        }finally{ 
            layingOut = false; 
        } 
    } 

    public Dimension getSize(){ 
        Dimension dim = super.getSize(); 
        if(!layingOut) 
            dim.width = Math.max(dim.width, getPreferredSize().width); 
        return dim; 
    } 
    
    public void setTextArea(JTextArea textArea)
    {
    	_textArea=textArea;
    	AttachListener(_textArea);
    }
    
	public void setTextField(JTextField textField)
	{
		_textField=textField;
    	AttachListener(_textField);
	}
	
	private void AttachListener(java.awt.Component objComponent)
	{
		objComponent.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				if(arg0.isAltDown())
				{
					if(arg0.getKeyChar()=='a')
						_textField.replaceSelection("ā");
					
					if(arg0.getKeyChar()=='i')
						_textField.replaceSelection("ī");
					
					if(arg0.getKeyChar()=='u')
						_textField.replaceSelection("ū");
					
					if(arg0.getKeyChar()=='t')
						_textField.replaceSelection("ṭ");
					
					if(arg0.getKeyChar()=='n')
						_textField.replaceSelection("ñ");

					if(arg0.getKeyChar()=='m')
						_textField.replaceSelection("ṃ");

					if(arg0.getKeyChar()=='g')
						_textField.replaceSelection("ṇ");

					if(arg0.getKeyChar()=='j')
						_textField.replaceSelection("ṅ");

					if(arg0.getKeyChar()=='l')
						_textField.replaceSelection("ḷ");

					if(arg0.getKeyChar()=='d')
						_textField.replaceSelection("ḍ");					
				}
				
			}

			@Override
			public void keyPressed(KeyEvent e) {}

			@Override
			public void keyReleased(KeyEvent e) {}
			
		});
	}
	
	@Override
	public void popupMenuWillBecomeInvisible(PopupMenuEvent arg0) {
		if(getSelectedItem()!=null)
		{
			if(_textField!=null)
				_textField.replaceSelection(((PaliKey)getSelectedItem()).KeyName);
			
			if(_textArea!=null)
				_textArea.replaceSelection(((PaliKey)getSelectedItem()).KeyName);
		}
	}

	@Override
	public void popupMenuCanceled(PopupMenuEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	private class PaliKey
	{
		public String KeyName;
		public String KeyValue;
		
		public PaliKey(String _KeyName,String _KeyValue)
		{
			KeyName=_KeyName;
			KeyValue=_KeyValue;
		}
		
		public String toString()
		{
			return KeyValue;
		}
	}
}
