package su.theravada.jpalireader.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class AppState implements Serializable {
	
	public int splitterLocation;
	public ArrayList<OpenBookInfo> arrOpenBooks;
	public ArrayList<BookmarkInfo> arrBookmarks;
	public ArrayList<LastBookInfo> arrLastBooks;
	
	public AppState()
	{
		arrOpenBooks=new ArrayList<OpenBookInfo>();
		arrBookmarks=new ArrayList<BookmarkInfo>();
		arrLastBooks=new ArrayList<LastBookInfo>();
	}		
	
	public class OpenBookInfo implements Serializable
	{
		public int nodeId;
		public int scrollPos;
		public int paneIndex;
		
		public OpenBookInfo()
		{
		}	
		
		public OpenBookInfo(int _nodeId,int _scrollPos,int _paneIndex)
		{
			nodeId=_nodeId;
			scrollPos=_scrollPos;
			paneIndex=_paneIndex;			
		}
	}	
	
	public class BookmarkInfo implements Serializable
	{
		public int nodeId;
		public int scrollPos;
		public String bookmarkName;
		
		public BookmarkInfo()
		{
			
		}
		
		public BookmarkInfo(int _nodeID,int _scrollPos,String _bookmarkName)
		{
			nodeId=_nodeID;
			scrollPos=_scrollPos;
			bookmarkName=_bookmarkName;
		}
		
		public String toString()
		{
			return bookmarkName;
		}
	}
	
	public class LastBookInfo implements Serializable
	{
		public int nodeId;
		public int scrollPos;
		public String bookName;
		
		public LastBookInfo()
		{
			
		}
		
		public LastBookInfo(int _nodeID,int _scrollPos,String _bookName)
		{
			nodeId=_nodeID;
			scrollPos=_scrollPos;
			bookName=_bookName;
		}
		
		public String toString()
		{
			return bookName;
		}
	}
}

