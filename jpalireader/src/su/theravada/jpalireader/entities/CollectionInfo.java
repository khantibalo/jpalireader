package su.theravada.jpalireader.entities;

public class CollectionInfo
{
	private int _collectionId;
	private String _title;
	private boolean _hasPTSPages;
	private int _parentId;
	
	public CollectionInfo(int CollectionID,String Title,boolean HasPTSPages)
	{
		_collectionId=CollectionID;
		_title=Title;
		_hasPTSPages=HasPTSPages;
	}

	public CollectionInfo(int CollectionID,String Title,boolean HasPTSPages,int ParentID)
	{
		_collectionId=CollectionID;
		_title=Title;
		_hasPTSPages=HasPTSPages;
		_parentId=ParentID;
	}
	
	public String toString()
	{
		return hasPTSPages() ? "  "+_title : _title;
	}

	public boolean hasPTSPages() {
		return _hasPTSPages;
	}		
	
	public int getCollectionID()
	{
		return _collectionId;
	}
	
	public int getParentID()
	{
		return _parentId;
	}
}
