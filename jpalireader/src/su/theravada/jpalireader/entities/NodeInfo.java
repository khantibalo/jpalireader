package su.theravada.jpalireader.entities;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import su.theravada.jpalireader.MainWindow;
import su.theravada.jpalireader.util.SqlUtil;

public class NodeInfo
{
	private int _nodeID;
	private String _nodeTitle;
	private int _FileID;
	
	public NodeInfo(int nNodeID,String strNodeTitle,int FileID)
	{
		_nodeID = nNodeID;
		_nodeTitle=strNodeTitle;
		_FileID=FileID;
	}

    public String toString() {
        return _nodeTitle;
    }

	public int getNodeID() {
		return _nodeID;
	}

	public int getFileID() {
		return _FileID;
	}
	
	public String getPath()
	{
		StringBuilder objSB=new StringBuilder();

		ResultSet rs = null;
        PreparedStatement psSelect = null;
        
		try
		{	
			int NodeID=getNodeID();
			
			do
			{
				psSelect=MainWindow.getDBConnection().prepareStatement("SELECT nodetitle,parentid FROM nodes WHERE nodeid=?");
				psSelect.setInt(1, NodeID);
				
				rs = psSelect.executeQuery();
				if(rs.next())
				{
					objSB.insert(0, rs.getString("nodetitle")+"/ ");
					NodeID=rs.getInt("parentid");					
				}								
			}
			while(NodeID>0);
            
		}
		catch(SQLException sqle)
		{		
			SqlUtil.ReportSQLException(sqle);
		}
		catch(Exception ex)
		{		
			ex.printStackTrace();					
		}
		finally
		{
			SqlUtil.ClosePreparedStatement(psSelect);
			SqlUtil.CloseResultSet(rs);
		}
		
		String strPath=objSB.toString();
		
		return strPath.substring(0,strPath.length()-2);
	}
}
