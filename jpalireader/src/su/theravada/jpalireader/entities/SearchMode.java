package su.theravada.jpalireader.entities;

public class SearchMode {
	String _Name;
	int _id;
	
	public SearchMode(String strName,int id)
	{
		_Name=strName;
		_id=id;
	}
	
	public String toString()
	{
		return _Name;
	}
	
	public int getId()
	{
		return _id;
	}
}
