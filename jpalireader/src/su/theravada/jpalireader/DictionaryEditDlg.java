package su.theravada.jpalireader;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import su.theravada.jpalireader.util.SqlUtil;
import su.theravada.jpalireader.util.UTF8Control;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class DictionaryEditDlg extends javax.swing.JDialog {
	private JLabel ctlTermNameLabel;
	private JTextField ctlTermName;
	private JLabel ctlTranslationLabel;
	private JTextArea ctlTranslation;
	private JButton ctlCancel;
	private JButton ctlOK;
	private PaliComboBox ctlTranslationPali;
	private PaliComboBox ctlTermNamePali;
	private int _entryId;
	
	public boolean DialogSuccess;
	
	/**
	* Auto-generated main method to display this JDialog
	*/
		
	public DictionaryEditDlg(JFrame frame) {
		super(frame);
		initGUI();
	}
	
	public DictionaryEditDlg(JFrame frame,int entryID) {
		super(frame);
		initGUI();
		_entryId=entryID;
		
		DisplayEntry();
	}	
	
	private void DisplayEntry()
	{
		ResultSet rs = null;
        PreparedStatement ps = null;
        
        try
        {
        	ps=MainWindow.getDBConnection().prepareStatement("SELECT entryid, keyword, \"Translation\" FROM dictionarycontent WHERE entryid=?");
			ps.setInt(1, _entryId);			
			rs=ps.executeQuery();
        	
			if(rs.next())
			{
				ctlTermName.setText(rs.getString("keyword"));
				ctlTranslation.setText(rs.getString("translation").replace("[c]", "").replace("[/c]", "\r\n"));
			}        	
        }
		catch(SQLException sqle)
		{		
			SqlUtil.ReportSQLException(sqle);
		}
		catch(Exception ex)
		{		
			ex.printStackTrace();					
		}
		finally
		{
			SqlUtil.ClosePreparedStatement(ps);
			SqlUtil.CloseResultSet(rs);		
		}
	}
	
	private void initGUI() {
		try {
	        
			ResourceBundle res_strings = ResourceBundle.getBundle("strings", Locale.getDefault(),new UTF8Control());
			{
				getContentPane().setLayout(null);
				this.setModal(true);
				this.setResizable(false);
			}
			{
				ctlTermNameLabel = new JLabel();
				getContentPane().add(ctlTermNameLabel, "Center");
				ctlTermNameLabel.setText(res_strings.getString("TermName"));
				ctlTermNameLabel.setBounds(6, 5, 179, 20);
			}
			{
				ctlTermName = new JTextField();
				getContentPane().add(ctlTermName);
				ctlTermName.setBounds(6, 26, 319, 22);
			}
			{
				ctlTermNamePali=new PaliComboBox();
				getContentPane().add(ctlTermNamePali);
				ctlTermNamePali.setBounds(331, 25, 25, 22);
			}
			{
				ctlTranslationLabel = new JLabel();
				getContentPane().add(ctlTranslationLabel);
				ctlTranslationLabel.setText(res_strings.getString("Translation"));
				ctlTranslationLabel.setBounds(6, 60, 170, 15);
			}
			{
				ctlTranslation = new JTextArea();
				getContentPane().add(ctlTranslation);
				ctlTranslation.setBounds(6, 81, 319, 114);
				ctlTranslation.setBorder(new LineBorder(new java.awt.Color(0,0,0), 1, false));
			}
			{
				ctlTranslationPali = new PaliComboBox();
				getContentPane().add(ctlTranslationPali);
				ctlTranslationPali.setBounds(331, 87, 25, 22);
			}
			{
				ctlOK = new JButton();
				getContentPane().add(ctlOK);
				ctlOK.setText(res_strings.getString("OK"));
				ctlOK.setBounds(163, 221, 80, 22);
				ctlOK.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						ctlOKActionPerformed(evt);
					}
				});
			}
			{
				ctlCancel = new JButton();
				getContentPane().add(ctlCancel);
				ctlCancel.setText(res_strings.getString("Cancel"));
				ctlCancel.setBounds(248, 221, 77, 22);
				ctlCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						ctlCancelActionPerformed(evt);
					}
				});
			}
			
			ctlTermNamePali.setTextField(ctlTermName);
			ctlTranslationPali.setTextArea(ctlTranslation);
			setTitle(res_strings.getString("EditDictionary"));
			this.setSize(376, 284);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void ctlOKActionPerformed(ActionEvent evt) {
		
        PreparedStatement ps = null;
        
        try
        {
        	if(_entryId>0)
        	{
        		ps=MainWindow.getDBConnection().prepareStatement("UPDATE dictionarycontent SET keyword=?, \"Translation\"=? WHERE entryid=?");
            	ps.setInt(3, _entryId);
        	}
        	else
        		ps=MainWindow.getDBConnection().prepareStatement("INSERT INTO dictionarycontent(entryid,keyword,\"Translation\") VALUES(NEXT VALUE FOR DictionaryEntryID,?,?)");
        	
        	ps.setString(1, ctlTermName.getText().toLowerCase());
        	ps.setString(2, ctlTranslation.getText());
        	
			ps.execute();      	
        }
		catch(SQLException sqle)
		{		
			SqlUtil.ReportSQLException(sqle);
		}
		catch(Exception ex)
		{		
			ex.printStackTrace();					
		}
		finally
		{
			SqlUtil.ClosePreparedStatement(ps);	
		}		
		
		DialogSuccess=true;
		setVisible(false);
	}
	
	private void ctlCancelActionPerformed(ActionEvent evt) {
		DialogSuccess=false;
		setVisible(false);
	}

}
