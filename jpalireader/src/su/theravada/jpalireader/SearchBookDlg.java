package su.theravada.jpalireader;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import su.theravada.jpalireader.util.UTF8Control;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class SearchBookDlg extends javax.swing.JDialog {
	private JLabel ctlSearchTextLabel;
	private PaliComboBox ctlPaliCombo;
	public JPanel ctlNavButtons;
	public JButton ctlLastMatch;
	public JButton ctlNextMatch;
	public JButton ctlPrevMatch;
	public JButton ctlFirstMatch;
	public JButton ctlClose;
	public JButton ctlFind;
	public JTextField ctlSearchText;

	public SearchBookDlg(JFrame frame) {
		super(frame);
		initGUI();
	}
	
	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				this.setResizable(false);
			}
			ResourceBundle res_strings = ResourceBundle.getBundle("strings", Locale.getDefault(),new UTF8Control());
			{
				ctlSearchTextLabel = new JLabel();
				getContentPane().add(ctlSearchTextLabel, "Center");
				ctlSearchTextLabel.setText(res_strings.getString("SearchText"));
				ctlSearchTextLabel.setBounds(4, 6, 94, 27);
			}
			{
				ctlSearchText = new JTextField();
				getContentPane().add(ctlSearchText, "North");
				ctlSearchText.setBounds(104, 9, 228, 22);
			}
			{
				ctlFind = new JButton();
				getContentPane().add(ctlFind);
				ctlFind.setText(res_strings.getString("Find"));
				ctlFind.setBounds(104, 43, 98, 22);
			}
			{
				ctlClose = new JButton();
				getContentPane().add(ctlClose);
				ctlClose.setText(res_strings.getString("Close"));
				ctlClose.setBounds(213, 43, 90, 22);				
			}
			{

				ctlPaliCombo = new PaliComboBox();
				getContentPane().add(ctlPaliCombo);
				ctlPaliCombo.setBounds(338, 8, 38, 22);
				ctlPaliCombo.setTextField(ctlSearchText);
			}
			{
				ctlNavButtons = new JPanel();
				getContentPane().add(ctlNavButtons);
				ctlNavButtons.setBounds(0, 70, 435, 32);
				ctlNavButtons.setVisible(false);
				{
					ctlFirstMatch = new JButton();
					ctlNavButtons.add(ctlFirstMatch);
					ctlFirstMatch.setText(res_strings.getString("First"));
					ctlFirstMatch.setBounds(39, 78, 65, 22);
				}
				{
					ctlPrevMatch = new JButton();
					ctlNavButtons.add(ctlPrevMatch);
					ctlPrevMatch.setText(res_strings.getString("Prev"));
					ctlPrevMatch.setBounds(115, 78, 65, 22);
				}
				{
					ctlNextMatch = new JButton();
					ctlNavButtons.add(ctlNextMatch);
					ctlNextMatch.setText(res_strings.getString("Next"));
					ctlNextMatch.setBounds(191, 78, 66, 22);
				}
				{
					ctlLastMatch = new JButton();
					ctlNavButtons.add(ctlLastMatch);
					ctlLastMatch.setText(res_strings.getString("Last"));
					ctlLastMatch.setBounds(268, 78, 64, 22);
				}
			}
			this.setSize(445, 128);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	

}
