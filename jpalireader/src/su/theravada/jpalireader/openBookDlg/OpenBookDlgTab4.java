package su.theravada.jpalireader.openBookDlg;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListModel;

import su.theravada.jpalireader.MainWindow;
import su.theravada.jpalireader.entities.AppState;
import su.theravada.jpalireader.entities.NodeInfo;
import su.theravada.jpalireader.util.UTF8Control;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class OpenBookDlgTab4 extends javax.swing.JPanel {
	private JScrollPane ctlScrollPane;
	private JList ctlBookmarksList;
	private JButton ctlEditBookmark;
	private JButton ctlDeleteBookmark;
	private MainWindow _frame;
	
	public OpenBookDlgTab4(JFrame frame) {
		super();
		_frame=(MainWindow)frame;
		initGUI();
	}
	
	private void initGUI() {
		try {
			ResourceBundle res_strings = ResourceBundle.getBundle("strings", Locale.getDefault(),new UTF8Control());
			this.setPreferredSize(new java.awt.Dimension(424, 284));
			this.setLayout(null);
			{
				ctlScrollPane = new JScrollPane();
				this.add(ctlScrollPane);
				ctlScrollPane.setBounds(0, 1, 530, 231);
				{
					ListModel ctlBookmarksListModel = 
						new DefaultComboBoxModel(_frame.appState.arrBookmarks.toArray());
					ctlBookmarksList = new JList();
					ctlScrollPane.setViewportView(ctlBookmarksList);
					ctlBookmarksList.setModel(ctlBookmarksListModel);
					ctlBookmarksList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				}
			}
			{
				ctlEditBookmark = new JButton();
				this.add(ctlEditBookmark);
				ctlEditBookmark.setText(res_strings.getString("Edit"));
				ctlEditBookmark.setBounds(29, 243, 103, 22);
				ctlEditBookmark.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						ctlEditBookmarkActionPerformed(evt);
					}
				});
			}
			{
				ctlDeleteBookmark = new JButton();
				this.add(ctlDeleteBookmark);
				ctlDeleteBookmark.setText(res_strings.getString("Delete"));
				ctlDeleteBookmark.setBounds(263, 243, 119, 22);
				ctlDeleteBookmark.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						ctlDeleteBookmarkActionPerformed(evt);
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getNodeID() {
		int RetVal=0;
		
		if(ctlBookmarksList.getSelectedValue()!=null)
		{
			AppState.BookmarkInfo objBookmark=(AppState.BookmarkInfo)ctlBookmarksList.getSelectedValue();
			
			RetVal=objBookmark.nodeId;			
		}		
		
		return RetVal;
	}	
	
	public int getScrollPos()
	{
		int RetVal=0;
		
		if(ctlBookmarksList.getSelectedValue()!=null)
		{
			AppState.BookmarkInfo objBookmark=(AppState.BookmarkInfo)ctlBookmarksList.getSelectedValue();
			
			RetVal=objBookmark.scrollPos;			
		}		
		
		return RetVal;		
	}
	
	private void ctlEditBookmarkActionPerformed(ActionEvent evt) 
	{
		if(ctlBookmarksList.getSelectedValue()!=null)
		{
			AppState.BookmarkInfo objBookmark=(AppState.BookmarkInfo)ctlBookmarksList.getSelectedValue();
			
			ResourceBundle res_strings = ResourceBundle.getBundle("strings", Locale.getDefault(),new UTF8Control());
			
			String strBookMarkName = (String)JOptionPane.showInputDialog(
					_frame,
					res_strings.getString("EnterBookmarkName"),"",
	                JOptionPane.PLAIN_MESSAGE,null,null,objBookmark.bookmarkName);
			
			if(strBookMarkName!=null && !strBookMarkName.isEmpty())
			{
				objBookmark.bookmarkName=strBookMarkName;
				ctlBookmarksList.repaint();
			}
		}	
	}
	
	private void ctlDeleteBookmarkActionPerformed(ActionEvent evt) 
	{
		ResourceBundle res_strings = ResourceBundle.getBundle("strings", Locale.getDefault(),new UTF8Control());

		int n = JOptionPane.showConfirmDialog(
				_frame,res_strings.getString("SureDeleteItem"), res_strings.getString("Confirm"),
			    JOptionPane.YES_NO_OPTION);

		if(n==JOptionPane.YES_OPTION)
		{
			AppState.BookmarkInfo objBookmark=(AppState.BookmarkInfo)ctlBookmarksList.getSelectedValue();
			((DefaultComboBoxModel)ctlBookmarksList.getModel()).removeElement(objBookmark);
			_frame.appState.arrBookmarks.remove(objBookmark);			
		}
	}
	
	public void RefreshBookmarksList()
	{
		ListModel ctlBookmarksListModel = 
			new DefaultComboBoxModel(_frame.appState.arrBookmarks.toArray());
		ctlBookmarksList.setModel(ctlBookmarksListModel);
	}
}
