package su.theravada.jpalireader.openBookDlg;

import java.awt.Dimension;
import javax.swing.DefaultComboBoxModel;

import javax.swing.WindowConstants;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.ListModel;

import su.theravada.jpalireader.MainWindow;
import su.theravada.jpalireader.entities.AppState;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class OpenBookDlgTab5 extends javax.swing.JScrollPane {
	private JList ctlLastBooksList;
	private MainWindow _frame;
	
	public OpenBookDlgTab5(JFrame frame) {
		super();
		_frame=(MainWindow)frame;
		initGUI();
	}
	
	private void initGUI() {
		try {
			setPreferredSize(new Dimension(400, 300));
			{
				ListModel ctlLastBooksListModel = 
					new DefaultComboBoxModel(_frame.appState.arrLastBooks.toArray());
				ctlLastBooksList = new JList();
				this.setViewportView(ctlLastBooksList);
				ctlLastBooksList.setModel(ctlLastBooksListModel);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getNodeID() {
		int RetVal=0;
		
		if(ctlLastBooksList.getSelectedValue()!=null)
		{
			AppState.LastBookInfo objBookmark=(AppState.LastBookInfo)ctlLastBooksList.getSelectedValue();
			
			RetVal=objBookmark.nodeId;			
		}		
		
		return RetVal;
	}	
	
	public int getScrollPos()
	{
		int RetVal=0;
		
		if(ctlLastBooksList.getSelectedValue()!=null)
		{
			AppState.LastBookInfo objBookmark=(AppState.LastBookInfo)ctlLastBooksList.getSelectedValue();
			
			RetVal=objBookmark.scrollPos;			
		}		
		
		return RetVal;		
	}
	
	public void RefreshBooksList()
	{
		ListModel ctlBookmarksListModel = 
			new DefaultComboBoxModel(_frame.appState.arrLastBooks.toArray());
		ctlLastBooksList.setModel(ctlBookmarksListModel);
	}
}
