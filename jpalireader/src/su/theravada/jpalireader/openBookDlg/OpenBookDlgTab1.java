package su.theravada.jpalireader.openBookDlg;

import java.awt.BorderLayout;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.ExpandVetoException;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import su.theravada.jpalireader.MainWindow;
import su.theravada.jpalireader.entities.NodeInfo;
import su.theravada.jpalireader.util.SqlUtil;




/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class OpenBookDlgTab1 extends javax.swing.JScrollPane {
	private JTree tree;
	
	public OpenBookDlgTab1() {
		super();
		Fill(0);				
	}
	
	public OpenBookDlgTab1(int NodeID) {
		super();
		Fill(NodeID);				
	}	
	
	private void Fill(int NodeID)
	{
		DefaultMutableTreeNode top =null;								
        Statement s = null;
        ResultSet rs = null;
        PreparedStatement psSelect = null;
        
		try
		{		
            s = MainWindow.getDBConnection().createStatement();
			
			if(NodeID==0)
			{
				top =new DefaultMutableTreeNode("Tipitaka");
	            rs = s.executeQuery("SELECT nodeid, nodetitle,HasChildren FROM nodes WHERE ParentID IS NULL");				
			}
			else
			{
				psSelect = MainWindow.getDBConnection().prepareStatement("SELECT nodeid, nodetitle FROM nodes WHERE NodeID=?");
	            psSelect.setInt(1, NodeID);
	            rs = psSelect.executeQuery();	           
	            rs.next();
            	NodeInfo objNode=new NodeInfo(rs.getInt("nodeid"),rs.getString("nodetitle"),0);
            	top=new DefaultMutableTreeNode(objNode);
            	
            	psSelect = MainWindow.getDBConnection().prepareStatement("SELECT nodeid, nodetitle,HasChildren FROM nodes WHERE ParentID=?");
	            psSelect.setInt(1, NodeID);
	            rs = psSelect.executeQuery();
			}
			
            while(rs.next()) 
            {
            	NodeInfo objNode=new NodeInfo(rs.getInt("nodeid"),rs.getString("nodetitle"),0);
            	DefaultMutableTreeNode objTreeNode=new DefaultMutableTreeNode(objNode);            	
            	top.add(objTreeNode);
            	if(rs.getBoolean("HasChildren"))
            	{
            		DefaultMutableTreeNode objDummyNode=new DefaultMutableTreeNode(new NodeInfo(-1,"",0));            		
            		objTreeNode.add(objDummyNode);
            	}            	
            }                            
		}
		catch(SQLException sqle)
		{		
			SqlUtil.ReportSQLException(sqle);
		}
		catch(Exception e)
		{		
			e.printStackTrace();					
		}
		finally
		{
			SqlUtil.CloseResultSet(rs);
			SqlUtil.CloseStatement(s);
			SqlUtil.ClosePreparedStatement(psSelect);
		}
		
        //Create a tree that allows one selection at a time.

        //Create the scroll pane and add the tree to it. 

    	tree = new JTree(top);
    	this.setViewportView(tree);
    	tree.getSelectionModel().setSelectionMode
    	(TreeSelectionModel.SINGLE_TREE_SELECTION);
    	tree.addTreeWillExpandListener(new TreeWillExpandListener() {

			@Override
			public void treeWillCollapse(TreeExpansionEvent arg0)
					throws ExpandVetoException {				
			}

			@Override
			public void treeWillExpand(TreeExpansionEvent arg0)
					throws ExpandVetoException {
				TreeWillExpand(arg0);				
			}});
	}
	
	private void TreeWillExpand(TreeExpansionEvent e) throws ExpandVetoException {
		
		DefaultMutableTreeNode objTreeNode=(DefaultMutableTreeNode) e.getPath().getLastPathComponent();
		if(objTreeNode.getChildCount()>0)
		{
			DefaultMutableTreeNode objChildTreeNode=(DefaultMutableTreeNode) objTreeNode.getFirstChild();
			if(((NodeInfo)objChildTreeNode.getUserObject()).getNodeID()==-1)
			{				
				objTreeNode.removeAllChildren();
				
		        ResultSet rs = null;
		        PreparedStatement psSelect = null;
		        
				try
				{			
		            psSelect=MainWindow.getDBConnection().prepareStatement("SELECT nodeid, nodetitle,HasChildren,FileID FROM nodes WHERE ParentID=?");
		            psSelect.setInt(1, ((NodeInfo)objTreeNode.getUserObject()).getNodeID());
		            
		            rs = psSelect.executeQuery();
		            
		            while(rs.next()) 
		            {
		            	NodeInfo objNode=new NodeInfo(rs.getInt("nodeid"),rs.getString("nodetitle"),rs.getInt("FileID"));
		            	objChildTreeNode=new DefaultMutableTreeNode(objNode);            	
		            	objTreeNode.add(objChildTreeNode);
		            	if(rs.getBoolean("HasChildren"))
		            	{
		            		DefaultMutableTreeNode objDummyNode=new DefaultMutableTreeNode(new NodeInfo(-1,"",0));            		
		            		objChildTreeNode.add(objDummyNode);
		            	}            	
		            }                            
				}
				catch(SQLException sqle)
				{		
					SqlUtil.ReportSQLException(sqle);
				}
				catch(Exception ex)
				{		
					ex.printStackTrace();					
				}
				finally
				{
					SqlUtil.ClosePreparedStatement(psSelect);
					SqlUtil.CloseResultSet(rs);
				}
			}
		}
	}		

	public int getNodeID() {
		int RetVal=0;
		TreePath objPath=tree.getSelectionPath();

		if(objPath!=null)
		{
			DefaultMutableTreeNode objTreeNode=(DefaultMutableTreeNode) objPath.getLastPathComponent();
			NodeInfo objNode=(NodeInfo)objTreeNode.getUserObject();
			
			if(objNode.getFileID()!=0)
				RetVal=objNode.getNodeID();			
		}		
		
		return RetVal;
	}
}
