package su.theravada.jpalireader.openBookDlg;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.ComboBoxModel;

import javax.swing.JButton;
import javax.swing.JComboBox;

import javax.swing.ComponentInputMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import su.theravada.jpalireader.util.UTF8Control;


/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class OpenBookDlg extends javax.swing.JDialog {
	private JTabbedPane ctlTabs;
	private JButton ctlOK;
	private JLabel ctlTargetPaneLabel;
	private JComboBox ctlTargetPane;
	private JButton ctlCancel;
	private OpenBookDlgTab1 ctlTab1;
	private OpenBookDlgTab2 ctlTab2;
	private OpenBookDlgTab3 ctlTab3;
	private OpenBookDlgTab4 ctlTab4;
	private OpenBookDlgTab5 ctlTab5;
	
	private boolean _bDialogSuccess;
	private JFrame _frame;
	
	/**
	* Auto-generated main method to display this JDialog
	*/
		
	public OpenBookDlg(JFrame frame) {
		super(frame,true);
		_frame=frame;
		initGUI();
		
		final ResourceBundle res_strings = ResourceBundle.getBundle("strings", Locale.getDefault(),new UTF8Control());
		ctlTab1=new OpenBookDlgTab1();
		ctlTabs.add(res_strings.getString("Contents"), ctlTab1);
					
		ctlTab2=new OpenBookDlgTab2();
		ctlTabs.add(res_strings.getString("SearchNames"),ctlTab2);
		
		ctlTab3=new OpenBookDlgTab3();
		ctlTabs.add(res_strings.getString("OpenPTS"),ctlTab3);		
		
		ctlTab4=new OpenBookDlgTab4(_frame);
		ctlTabs.add(res_strings.getString("Bookmarks"),ctlTab4);	

		ctlTab5=new OpenBookDlgTab5(_frame);
		ctlTabs.add(res_strings.getString("History"),ctlTab5);	
	}
	
	public OpenBookDlg(JFrame frame,int NodeID)
	{
		super(frame,true);
		_frame=frame;
		initGUI();
		
		final ResourceBundle res_strings = ResourceBundle.getBundle("strings", Locale.getDefault(),new UTF8Control());
		ctlTab1=new OpenBookDlgTab1(NodeID);
		ctlTabs.add(res_strings.getString("Contents"), ctlTab1);
		
		ctlTab3=new OpenBookDlgTab3(NodeID);
		ctlTabs.add(res_strings.getString("OpenPTS"),ctlTab3);	
		
		ctlTargetPane.setVisible(false);
		ctlTargetPaneLabel.setVisible(false);
	}
	
	private void initGUI() {
		try {
			{
				getContentPane().setLayout(null);
				this.setModal(true);
				this.setResizable(false);
			}
			final ResourceBundle res_strings = ResourceBundle.getBundle("strings", Locale.getDefault(),new UTF8Control());
			{
				ctlTabs = new JTabbedPane();
				getContentPane().add(ctlTabs, "North");
				ctlTabs.setBounds(0, 0, 540, 365);
			}
			{
				ctlOK = new JButton();
				getContentPane().add(ctlOK, "West");
				ctlOK.setText(res_strings.getString("OK"));
				ctlOK.setBounds(0, 377, 123, 34);
				ctlOK.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						if(getNodeID()>0)
						{
							_bDialogSuccess=true;
							setVisible(false);
						}
						else
						{
							JOptionPane.showMessageDialog(_frame,
									res_strings.getString("BookNotSelected"),
								    res_strings.getString("Error"),
								    JOptionPane.ERROR_MESSAGE);
						}
					}
				});
			}
			{
				ctlCancel = new JButton();
				getContentPane().add(ctlCancel, "East");				
				ctlCancel.setText(res_strings.getString("Cancel"));
				ctlCancel.setBounds(431, 377, 109, 33);
				ctlCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						_bDialogSuccess=false;
						setVisible(false);
					}
				});
				
				ComboBoxModel ctlTargetPaneModel = 
					new DefaultComboBoxModel(
							new String[] { res_strings.getString("TopPane"), 
									res_strings.getString("BottomPane") });
				ctlTargetPane = new JComboBox();
				ctlTargetPane.setModel(ctlTargetPaneModel);
				ctlTargetPane.setBounds(221, 386, 98, 20);
				getContentPane().add(ctlTargetPane, "West");
				
				ctlTargetPaneLabel = new JLabel();
				ctlTargetPaneLabel.setText(res_strings.getString("OpenIn"));
				ctlTargetPaneLabel.setBounds(147, 389, 69, 15);	
				getContentPane().add(ctlTargetPaneLabel);
			}
			setSize(550, 450);
			
			addComponentListener(new ComponentListener() {
				@Override
				public void componentHidden(ComponentEvent arg0) {}

				@Override
				public void componentMoved(ComponentEvent arg0) {}

				@Override
				public void componentResized(ComponentEvent arg0) {}

				@Override
				public void componentShown(ComponentEvent arg0) {
					if(ctlTab4!=null)
						ctlTab4.RefreshBookmarksList();		
					
					if(ctlTab5!=null)
						ctlTab5.RefreshBooksList();
				}								
			});
						
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getNodeID() 
	{
		int RetVal=0;
		
		if(ctlTabs.getSelectedComponent() instanceof OpenBookDlgTab1)
			RetVal=ctlTab1.getNodeID();				
		
		if(ctlTabs.getSelectedComponent() instanceof OpenBookDlgTab2)
			RetVal=ctlTab2.getNodeID();
		
		if(ctlTabs.getSelectedComponent() instanceof OpenBookDlgTab3)
			RetVal=ctlTab3.getNodeID();
		
		if(ctlTabs.getSelectedComponent() instanceof OpenBookDlgTab4)
			RetVal=ctlTab4.getNodeID();
		
		if(ctlTabs.getSelectedComponent() instanceof OpenBookDlgTab5)
			RetVal=ctlTab5.getNodeID();		
		
		return RetVal;
	}

	public String getPTSPage()
	{
		String RetVal="";
		
		if(ctlTabs.getSelectedComponent() instanceof OpenBookDlgTab3)
			RetVal=ctlTab3.getPTSPage();		
		
		return RetVal;
	}
	
	public int getScrollPos()
	{
		int ScrollPos=0;
		
		if(ctlTabs.getSelectedComponent() instanceof OpenBookDlgTab4)
			ScrollPos=ctlTab4.getScrollPos();
		
		return ScrollPos;
	}
	
	
	public int getTargetPaneIndex()
	{
		return ctlTargetPane.getSelectedIndex();
	}
	
	public boolean isDialogSuccess() {
		return _bDialogSuccess;
	}

}
