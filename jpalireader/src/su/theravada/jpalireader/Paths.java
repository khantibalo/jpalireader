package su.theravada.jpalireader;

public class Paths {
	public static String CanonIndexPath="library/index/canon";
	public static String PEDIndexPath="library/index/ped";
	
	public static String CanonPath="library/canon.zip";
	public static String PEDPath="library/ped.zip";
}
